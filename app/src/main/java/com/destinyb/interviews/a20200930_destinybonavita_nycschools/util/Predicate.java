package com.destinyb.interviews.a20200930_destinybonavita_nycschools.util;

/**
 * Added to support all levels of Android
 */
public interface Predicate<T> {
    boolean test(T val);
}
