package com.destinyb.interviews.a20200930_destinybonavita_nycschools.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.destinyb.interviews.a20200930_destinybonavita_nycschools.R;
import com.destinyb.interviews.a20200930_destinybonavita_nycschools.util.AssetUtil;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.rxjava3.disposables.Disposable;

public class HomeFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    Disposable disposable;

    /// NOTE: Below contains the column names for the records
    /// TODO: Translate CSV to JSON for easier data handling
    public static final String headers = "dbn,school_name,boro,overview_paragraph,school_10th_seats,academicopportunities1,academicopportunities2,academicopportunities3,academicopportunities4,academicopportunities5,ell_programs,language_classes,advancedplacement_courses,diplomaendorsements,neighborhood,shared_space,campus_name,building_code,location,phone_number,fax_number,school_email,website,subway,bus,grades2018,finalgrades,total_students,start_time,end_time,addtl_info1,extracurricular_activities,psal_sports_boys,psal_sports_girls,psal_sports_coed,school_sports,graduation_rate,attendance_rate,pct_stu_enough_variety,college_career_rate,pct_stu_safe,girls,boys,pbat,international,specialized,transfer,ptech,earlycollege,geoeligibility,school_accessibility_description,prgdesc1,prgdesc2,prgdesc3,prgdesc4,prgdesc5,prgdesc6,prgdesc7,prgdesc8,prgdesc9,prgdesc10,directions1,directions2,directions3,directions4,directions5,directions6,directions7,directions8,directions9,directions10,requirement1_1,requirement1_2,requirement1_3,requirement1_4,requirement1_5,requirement1_6,requirement1_7,requirement1_8,requirement1_9,requirement1_10,requirement2_1,requirement2_2,requirement2_3,requirement2_4,requirement2_5,requirement2_6,requirement2_7,requirement2_8,requirement2_9,requirement2_10,requirement3_1,requirement3_2,requirement3_3,requirement3_4,requirement3_5,requirement3_6,requirement3_7,requirement3_8,requirement3_9,requirement3_10,requirement4_1,requirement4_2,requirement4_3,requirement4_4,requirement4_5,requirement4_6,requirement4_7,requirement4_8,requirement4_9,requirement4_10,requirement5_1,requirement5_2,requirement5_3,requirement5_4,requirement5_5,requirement5_6,requirement5_7,requirement5_8,requirement5_9,requirement5_10,requirement6_1,requirement6_2,requirement6_3,requirement6_4,requirement6_5,requirement6_6,requirement6_7,requirement6_8,requirement6_9,requirement6_10,requirement7_1,requirement7_2,requirement7_3,requirement7_4,requirement7_5,requirement7_6,requirement7_7,requirement7_8,requirement7_9,requirement7_10,requirement8_1,requirement8_2,requirement8_3,requirement8_4,requirement8_5,requirement8_6,requirement8_7,requirement8_8,requirement8_9,requirement8_10,requirement9_1,requirement9_2,requirement9_3,requirement9_4,requirement9_5,requirement9_6,requirement9_7,requirement9_8,requirement9_9,requirement9_10,requirement10_1,requirement10_2,requirement10_3,requirement10_4,requirement10_5,requirement10_6,requirement10_7,requirement10_8,requirement10_9,requirement10_10,requirement11_1,requirement11_2,requirement11_3,requirement11_4,requirement11_5,requirement11_6,requirement11_7,requirement11_8,requirement11_9,requirement11_10,requirement12_1,requirement12_2,requirement12_3,requirement12_4,requirement12_5,requirement12_6,requirement12_7,requirement12_8,requirement12_9,requirement12_10,offer_rate1,offer_rate2,offer_rate3,offer_rate4,offer_rate5,offer_rate6,offer_rate7,offer_rate8,offer_rate9,offer_rate10,program1,program2,program3,program4,program5,program6,program7,program8,program9,program10,code1,code2,code3,code4,code5,code6,code7,code8,code9,code10,interest1,interest2,interest3,interest4,interest5,interest6,interest7,interest8,interest9,interest10,method1,method2,method3,method4,method5,method6,method7,method8,method9,method10,seats9ge1,seats9ge2,seats9ge3,seats9ge4,seats9ge5,seats9ge6,seats9ge7,seats9ge8,seats9ge9,seats9ge10,grade9gefilledflag1,grade9gefilledflag2,grade9gefilledflag3,grade9gefilledflag4,grade9gefilledflag5,grade9gefilledflag6,grade9gefilledflag7,grade9gefilledflag8,grade9gefilledflag9,grade9gefilledflag10,grade9geapplicants1,grade9geapplicants2,grade9geapplicants3,grade9geapplicants4,grade9geapplicants5,grade9geapplicants6,grade9geapplicants7,grade9geapplicants8,grade9geapplicants9,grade9geapplicants10,seats9swd1,seats9swd2,seats9swd3,seats9swd4,seats9swd5,seats9swd6,seats9swd7,seats9swd8,seats9swd9,seats9swd10,grade9swdfilledflag1,grade9swdfilledflag2,grade9swdfilledflag3,grade9swdfilledflag4,grade9swdfilledflag5,grade9swdfilledflag6,grade9swdfilledflag7,grade9swdfilledflag8,grade9swdfilledflag9,grade9swdfilledflag10,grade9swdapplicants1,grade9swdapplicants2,grade9swdapplicants3,grade9swdapplicants4,grade9swdapplicants5,grade9swdapplicants6,grade9swdapplicants7,grade9swdapplicants8,grade9swdapplicants9,grade9swdapplicants10,seats1specialized,seats2specialized,seats3specialized,seats4specialized,seats5specialized,seats6specialized,applicants1specialized,applicants2specialized,applicants3specialized,applicants4specialized,applicants5specialized,applicants6specialized,appperseat1specialized,appperseat2specialized,appperseat3specialized,appperseat4specialized,appperseat5specialized,appperseat6specialized,seats101,seats102,seats103,seats104,seats105,seats106,seats107,seats108,seats109,seats1010,admissionspriority11,admissionspriority12,admissionspriority13,admissionspriority14,admissionspriority15,admissionspriority16,admissionspriority17,admissionspriority18,admissionspriority19,admissionspriority110,admissionspriority21,admissionspriority22,admissionspriority23,admissionspriority24,admissionspriority25,admissionspriority26,admissionspriority27,admissionspriority28,admissionspriority29,admissionspriority210,admissionspriority31,admissionspriority32,admissionspriority33,admissionspriority34,admissionspriority35,admissionspriority36,admissionspriority37,admissionspriority38,admissionspriority39,admissionspriority310,admissionspriority41,admissionspriority42,admissionspriority43,admissionspriority44,admissionspriority45,admissionspriority46,admissionspriority47,admissionspriority48,admissionspriority49,admissionspriority410,admissionspriority51,admissionspriority52,admissionspriority53,admissionspriority54,admissionspriority55,admissionspriority56,admissionspriority57,admissionspriority58,admissionspriority59,admissionspriority510,admissionspriority61,admissionspriority62,admissionspriority63,admissionspriority64,admissionspriority65,admissionspriority66,admissionspriority67,admissionspriority68,admissionspriority69,admissionspriority610,admissionspriority71,admissionspriority72,admissionspriority73,admissionspriority74,admissionspriority75,admissionspriority76,admissionspriority77,admissionspriority78,admissionspriority79,admissionspriority710,eligibility1,eligibility2,eligibility3,eligibility4,eligibility5,eligibility6,eligibility7,eligibility8,eligibility9,eligibility10,auditioninformation1,auditioninformation2,auditioninformation3,auditioninformation4,auditioninformation5,auditioninformation6,auditioninformation7,auditioninformation8,auditioninformation9,auditioninformation10,common_audition1,common_audition2,common_audition3,common_audition4,common_audition5,common_audition6,common_audition7,common_audition8,common_audition9,common_audition10,grade9geapplicantsperseat1,grade9geapplicantsperseat2,grade9geapplicantsperseat3,grade9geapplicantsperseat4,grade9geapplicantsperseat5,grade9geapplicantsperseat6,grade9geapplicantsperseat7,grade9geapplicantsperseat8,grade9geapplicantsperseat9,grade9geapplicantsperseat10,grade9swdapplicantsperseat1,grade9swdapplicantsperseat2,grade9swdapplicantsperseat3,grade9swdapplicantsperseat4,grade9swdapplicantsperseat5,grade9swdapplicantsperseat6,grade9swdapplicantsperseat7,grade9swdapplicantsperseat8,grade9swdapplicantsperseat9,grade9swdapplicantsperseat10,primary_address_line_1,city,Postcode,state_code,Latitude,Longitude,Community Board,Council District,Census Tract,BIN,BBL,NTA,Borough";
    String[] records;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        /// Setup RecyclerView
        recyclerView.setAdapter(new Adapter());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        /// Load the data asynchronously to prevent UI lag
        loadData();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

        /// Stop loading if app goes to background or user rotates device
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    /**
     * Method used to load data.csv asynchronously and to notify the user
     */
    private void loadData() {
        disposable = AssetUtil.readAsset("data.csv", getActivity().getAssets()).subscribe(data -> {
            if (data != null) {
                /// Split the csv file into lines for each individual record
                records = data.split("\n");

                /// Display the loaded data
                recyclerView.getAdapter().notifyDataSetChanged();
            } else {
                Toast.makeText(getContext(), "Failed to load data...", Toast.LENGTH_LONG).show();
            }
        });
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.button)
        Button button;

        @BindView(R.id.infoTextView)
        TextView infoTextView;

        @BindView(R.id.nameTextView)
        TextView nameTextView;

        public Holder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(Holder.this, itemView);
        }
    }

    private class Adapter extends RecyclerView.Adapter<Holder> {
        @NonNull
        @Override
        public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new Holder(getLayoutInflater().inflate(R.layout.record_layout, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull Holder holder, int position) {
            String[] data = records[position].split("\\,");

            holder.nameTextView.setText(data[1]);
            holder.infoTextView.setText(data[3]);

            holder.button.setOnClickListener(view -> {
                FragmentManager manager = getActivity().getSupportFragmentManager();

                manager.beginTransaction()
                        .replace(R.id.layout, SATFragment.newInstance(data[1], data))
                        .commit();
            });
        }

        @Override
        public int getItemCount() {
            return records != null ? records.length : 0;
        }
    }
}