package com.destinyb.interviews.a20200930_destinybonavita_nycschools.ui.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.destinyb.interviews.a20200930_destinybonavita_nycschools.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PairView extends ConstraintLayout {
    @BindView(R.id.leftTextView)
    public TextView leftTextView;

    @BindView(R.id.rightTextView)
    public TextView rightTextView;

    public PairView(Context context) {
        super(context);
        init(null, 0);
    }

    public PairView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public PairView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Inflate View
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.pair_view, this, true);

        // Bind Views
        ButterKnife.bind(this, view);

        // Load attributes
        if (attrs != null) {
            final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.PairView, defStyle, 0);

            // Set values
            leftTextView.setText(a.getString(R.styleable.PairView_leftString));
            rightTextView.setText(a.getString(R.styleable.PairView_rightString));

            // Recycle attributes
            a.recycle();
        }
    }
}
