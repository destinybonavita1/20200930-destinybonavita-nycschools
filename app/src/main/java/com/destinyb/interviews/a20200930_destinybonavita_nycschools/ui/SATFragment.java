package com.destinyb.interviews.a20200930_destinybonavita_nycschools.ui;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.destinyb.interviews.a20200930_destinybonavita_nycschools.R;
import com.destinyb.interviews.a20200930_destinybonavita_nycschools.ui.components.PairView;
import com.destinyb.interviews.a20200930_destinybonavita_nycschools.util.ArrayUtil;
import com.destinyb.interviews.a20200930_destinybonavita_nycschools.util.AssetUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.rxjava3.disposables.Disposable;

public class SATFragment extends Fragment {
    private static final String ARG_PARAM = "schoolName";
    private String schoolName;

    private static final String ARG_PARAM1 = "schoolData";
    private String[] schoolData;

    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;

    @BindView(R.id.nameTextView)
    TextView nameTextView;

    Disposable disposable;

    /// The below are column names
    final String headers = "DBN,SCHOOL NAME,Num of SAT Test Takers,SAT Critical Reading Avg. Score,SAT Math Avg. Score,SAT Writing Avg. Score";
    String[] scores;

    public static SATFragment newInstance(String schoolName, String[] schoolData) {
        SATFragment fragment = new SATFragment();

        Bundle args = new Bundle();
        args.putString(ARG_PARAM, schoolName);
        args.putStringArray(ARG_PARAM1, schoolData);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            schoolName = getArguments().getString(ARG_PARAM);
            schoolData = getArguments().getStringArray(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sat, container, false);
        ButterKnife.bind(this, view);

        loadData();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    private void loadData() {
        nameTextView.setText(schoolName);

        AssetUtil.readAsset("sat_data.csv", getActivity().getAssets()).subscribe(data -> {
            if (data != null) {
                String[] records = data.split("\n");
                records = ArrayUtil.find(records, record -> {
                    return record.split("\\,")[1].equalsIgnoreCase(schoolName);
                });

                if (records.length > 0) {
                    scores = records[0].split("\\,");
                }

                displaySATData();
                displaySchoolData();
            } else {
                Toast.makeText(getContext(), "Failed to load SATs...", Toast.LENGTH_LONG).show();
            }
        });
    }

    /// NOTE: Normally I would use a recycler view but since there is two different data sources, for simplicity sake I'll manually add the views
    private void displaySATData() {
        if (scores != null) {
            String[] headers = this.headers.split("\\,");

            for (int i = 2; i < headers.length; i++) {
                PairView pairView = new PairView(getContext());

                pairView.leftTextView.setText(headers[i]);
                pairView.rightTextView.setText(scores[i]);

                linearLayout.addView(pairView);
                correctView(pairView);
            }
        } else {
            TextView textView = new TextView(getContext());
            textView.setGravity(Gravity.CENTER);

            /// Normally I'd add translations for this text
            textView.setText("No SAT scores found for this school");

            linearLayout.addView(textView);
            correctView(textView);
        }
    }

    private void displaySchoolData() {
        String[] headers = HomeFragment.headers.split("\\,");

        for (int i = 2; i < headers.length; i++) {
            if (schoolData[i].trim().length() > 0) {
                PairView pairView = new PairView(getContext());

                pairView.leftTextView.setText(headers[i]);
                pairView.rightTextView.setText(schoolData[i]);

                linearLayout.addView(pairView);
                correctView(pairView);
            }
        }
    }

    /**
     * Used to correct view sizes in LinearLayout
     *
     * @param view
     */
    private void correctView(View view) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();

        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
    }
}