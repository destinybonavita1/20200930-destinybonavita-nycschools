package com.destinyb.interviews.a20200930_destinybonavita_nycschools;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.destinyb.interviews.a20200930_destinybonavita_nycschools.ui.HomeFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setup();
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();

        if (manager.getFragments().size() > 1)
            manager.popBackStack();;
    }

    private void setup() {
        /// Get the fragment manager to assist navigation
        FragmentManager manager = getSupportFragmentManager();

        /// Navigate to the "Home" fragment
        manager.beginTransaction()
                .replace(R.id.layout, new HomeFragment())
                .commit();
    }
}