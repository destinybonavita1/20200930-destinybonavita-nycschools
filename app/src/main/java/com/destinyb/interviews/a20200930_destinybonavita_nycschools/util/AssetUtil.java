package com.destinyb.interviews.a20200930_destinybonavita_nycschools.util;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import io.reactivex.rxjava3.core.Single;

public class AssetUtil {
    public static Single<String> readAsset(String fileName, AssetManager manager) {
        return Single.fromCallable(() -> {
            /// Create a builder to start building the content
            StringBuilder builder = new StringBuilder();

            /// Read the asset using a BufferedReader
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(manager.open(fileName)))) {
                String line;

                /// Read each line accordingly
                while ((line = reader.readLine()) != null) {
                    /// Add the line to the builder
                    builder.append(line);

                    /// Since we're reading line by line, we're gonna need to append a line break at the end
                    builder.append('\n');
                }
            }

            /// Remove any empty lines
            builder.trimToSize();

            return builder.toString();
        }).onErrorReturn((err -> {
            Log.e("asset", String.format("Failed to read asset %s", fileName), err);

            return null;
        }));
    }
}
