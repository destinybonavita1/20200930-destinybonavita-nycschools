package com.destinyb.interviews.a20200930_destinybonavita_nycschools.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class ArrayUtil {
    public static String[] find(String[] data, Predicate<String> predict) {
        List<String> results = new ArrayList<>();

        for (String record : data) {
            if (predict.test(record)) {
                results.add(record);
            }
        }

        return results.toArray(new String[0]);
    }
}
